CLASS zraph_data_generator DEFINITION PUBLIC FINAL CREATE PUBLIC .
  PUBLIC SECTION.
    INTERFACES: if_oo_adt_classrun.
ENDCLASS.



CLASS ZRAPH_DATA_GENERATOR IMPLEMENTATION.


  METHOD if_oo_adt_classrun~main.
    TYPES hotel TYPE STANDARD TABLE OF zraph_hotel WITH KEY hotel_id.
    DATA roomreservations TYPE STANDARD TABLE OF zraph_roomrsv WITH DEFAULT KEY.
    DATA hotelsmaster TYPE hotel.

    " Clear personal travel table and fill it from /DMO/ reference table
    DELETE FROM zraph_travel.
    INSERT zraph_travel FROM ( SELECT * FROM /dmo/a_travel_d ).
    out->write( 'Travel data copied.' ).

    " Clear personal booking table and fill it from /DMO/ reference table
    DELETE FROM zraph_booking.
    INSERT zraph_booking FROM ( SELECT * FROM /dmo/a_booking_d ).
    out->write( 'Booking data copied.' ).

    " Clear personal booking supplement table and fill it from /DMO/ reference table
    DELETE FROM zraph_booksup.
    INSERT zraph_booksup FROM ( SELECT * FROM /dmo/a_bksuppl_d ).
    out->write( 'Booking supplement data copied.' ).

    " Clear and fill hotel master data
    hotelsmaster = VALUE hotel(
      ( hotel_id  = '000001'
        name      = 'Mothership'
        city      = 'Cupertino                '
        country   = 'US '
        )
      ( hotel_id  = '000002'
        name      = 'Grand Hotel'
        city      = 'Washington               '
        country   = 'US '
        )
      ( hotel_id  = '000013'
        name      = 'Trump Hotel'
        city      = 'New York                 '
        country   = 'US '
        )
      ( hotel_id  = '002037'
        name      = 'Beverly Hills Hotel'
        city      = 'Los Angeles              '
        country   = 'US '
        )
      ( hotel_id  = '108424'
        name      = 'The Plaza'
        city      = 'New York                 '
        country   = 'US '
        )
      ( hotel_id  = '000040'
        name      = 'Sunrise Beach'
        city      = 'San Francisco            '
        country   = 'US '
        )
      ( hotel_id  = '000075'
        name      = 'Sunshine Travel'
        city      = 'Munich                   '
        country   = 'DE '
        )
      ( hotel_id  = '000505'
        name      = 'Weisser Hase'
        city      = 'Passau                   '
        country   = 'DE '
        )
      ( hotel_id  = '008527'
        name      = 'Hotel Adlon'
        city      = 'Berlin                   '
        country   = 'DE '
        )
      ( hotel_id  = '000328'
        name      = 'Ibis Budget'
        city      = 'Hamburg                  '
        country   = 'DE '
        )
      ( hotel_id  = '001206'
        name      = 'Bamboo Garden'
        city      = 'Tokyo                    '
        country   = 'JP '
        )
      ( hotel_id  = '000068'
        name      = 'Sushi Palace'
        city      = 'Kyoto                    '
        country   = 'JP '
        )
      ( hotel_id  = '000736'
        name      = 'Cherry Blossom'
        city      = 'Yokohama                 '
        country   = 'JP '
        )
      ( hotel_id  = '000007'
        name      = 'British Yard'
        city      = 'London                   '
        country   = 'UK '
        )
      ( hotel_id  = '070007'
        name      = 'The Ritz Hotel'
        city      = 'London                   '
        country   = 'UK '
        )
      ( hotel_id  = '020307'
        name      = 'Castle View'
        city      = 'Edinburgh                '
        country   = 'UK '
        )
      ( hotel_id  = '000543'
        name      = 'Cliffside'
        city      = 'Belfast                  '
        country   = 'UK '
        )
      ( hotel_id  = '000258'
        name      = 'Mediterranean Palace'
        city      = 'Rome                     '
        country   = 'IT '
        )
      ( hotel_id  = '000168'
        name      = 'Hotel Santa Chiara'
        city      = 'Venice                   '
        country   = 'IT '
        )
      ( hotel_id  = '000815'
        name      = 'Firenze Porta Rossa'
        city      = 'Florence                 '
        country   = 'IT '
        )
      ( hotel_id  = '000378'
        name      = 'Vodka House'
        city      = 'Saint Petersburg         '
        country   = 'RU '
        )
      ( hotel_id  = '100348'
        name      = 'Kempinski'
        city      = 'Moscow                   '
        country   = 'RU '
        )
      ( hotel_id  = '000021'
        name      = 'Sunny Side'
        city      = 'Barcelona                '
        country   = 'ES '
        )
      ( hotel_id  = '054361'
        name      = 'Villa Magna'
        city      = 'Madrid                   '
        country   = 'ES '
        )
      ( hotel_id  = '020001'
        name      = 'The Eiffel'
        city      = 'Paris                    '
        country   = 'FR '
        )
      ( hotel_id  = '043567'
        name      = 'Hotel Ritz'
        city      = 'Paris                    '
        country   = 'FR '
        ) ).
    DELETE FROM zraph_hotel.
    INSERT zraph_hotel FROM TABLE @hotelsmaster.
    out->write( 'Hotel master data generated.' ).

    " Clear personal room reservation table and fill it from data generated based on existing travels and hotels
    SELECT COUNT( * ) FROM zraph_hotel INTO @DATA(hotel_count).
    IF hotel_count = 0.
      out->write( 'Aborted: No hotels found!' ).
      RETURN.
    ENDIF.
    DELETE FROM zraph_roomrsv.
    SELECT travel_uuid, begin_date, end_date, total_price, currency_code FROM zraph_travel INTO TABLE @DATA(travels).
    SELECT hotel_id FROM zraph_hotel INTO TABLE @DATA(hotels).
    LOOP AT travels ASSIGNING FIELD-SYMBOL(<travel>).
      DATA(index) = sy-tabix.
      READ TABLE hotels INDEX index MOD hotel_count + 1 INTO DATA(hotel).
      IF index MOD 4 <= 2.
        APPEND VALUE #( parent_uuid   = <travel>-travel_uuid
                        roomrsv_uuid  = cl_system_uuid=>create_uuid_x16_static( )
                        roomrsv_id    = '000001'
                        hotel_id      = hotel-hotel_id
                        begin_date    = <travel>-begin_date
                        end_date      = <travel>-end_date
                        room_type     = 'S'
                        roomrsv_price = <travel>-total_price * '0.15'
                        currency_code = <travel>-currency_code ) TO roomreservations.
        IF index MOD 4 = 1.
          APPEND VALUE #( parent_uuid   = <travel>-travel_uuid
                          roomrsv_uuid  = cl_system_uuid=>create_uuid_x16_static( )
                          roomrsv_id    = '000002'
                          hotel_id      = hotel-hotel_id
                          begin_date    = <travel>-begin_date
                          end_date      = <travel>-end_date
                          room_type     = 'D'
                          roomrsv_price = <travel>-total_price * '0.25'
                          currency_code = <travel>-currency_code ) TO roomreservations.
        ELSEIF index MOD 4 = 2.
          APPEND VALUE #( parent_uuid   = <travel>-travel_uuid
                          roomrsv_uuid  = cl_system_uuid=>create_uuid_x16_static( )
                          roomrsv_id    = '000002'
                          hotel_id      = hotel-hotel_id
                          begin_date    = <travel>-begin_date
                          end_date      = <travel>-end_date
                          room_type     = 'F'
                          roomrsv_price = <travel>-total_price * '0.4'
                          currency_code = <travel>-currency_code ) TO roomreservations.
        ENDIF.
      ENDIF.
      IF index MOD 4 = 3.
        APPEND VALUE #( parent_uuid   = <travel>-travel_uuid
                        roomrsv_uuid  = cl_system_uuid=>create_uuid_x16_static( )
                        roomrsv_id    = '000001'
                        hotel_id      = hotel-hotel_id
                        begin_date    = <travel>-begin_date
                        end_date      = <travel>-end_date
                        room_type     = 'E'
                        roomrsv_price = <travel>-total_price * '0.7'
                        currency_code = <travel>-currency_code ) TO roomreservations.
      ENDIF.
    ENDLOOP.
    INSERT zraph_roomrsv FROM TABLE @roomreservations.
    out->write( 'Room reservation data generated.' ).

  ENDMETHOD.
ENDCLASS.
