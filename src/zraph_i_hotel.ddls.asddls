@AbapCatalog.sqlViewName: 'ZRAPHIHOTEL'
@AbapCatalog.compiler.compareFilter: true
@AbapCatalog.preserveKey: true
@AccessControl.authorizationCheck: #CHECK
@EndUserText.label: 'RAP HandsOn: Hotel (DB)'
define view ZRAPH_I_HOTEL
  as select from zraph_hotel
{
  key hotel_id as HotelID,
      name     as Name,
      city     as City,
      country  as Country
}
