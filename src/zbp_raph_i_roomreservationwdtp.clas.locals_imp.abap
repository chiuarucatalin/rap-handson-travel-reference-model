CLASS lhc_roomreservation DEFINITION INHERITING FROM cl_abap_behavior_handler.
  PRIVATE SECTION.

    METHODS calculatetotalprice FOR DETERMINE ON MODIFY
      IMPORTING keys FOR roomreservation~calculatetotalprice.

ENDCLASS.

CLASS lhc_roomreservation IMPLEMENTATION.

  METHOD calculatetotalprice.
    READ ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
      ENTITY roomreservation BY \_travel
        FROM CORRESPONDING #( keys )
      LINK DATA(lt_link).

    MODIFY ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
      ENTITY travel
      EXECUTE recalctotalprice
      FROM VALUE #( FOR <link> IN lt_link ( COND #( WHEN <link>-target-%is_draft = if_abap_behv=>mk-off THEN VALUE #( traveluuid = <link>-target-traveluuid ) ) ) )
      REPORTED DATA(lt_reported).

    reported = CORRESPONDING #( DEEP lt_reported ).
  ENDMETHOD.

ENDCLASS.
