@AbapCatalog.viewEnhancementCategory: [#NONE]
@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'Travel Status'
@Metadata.ignorePropagatedAnnotations: true

@ObjectModel: { usageType:{
    serviceQuality: #X,
    sizeCategory: #S,
    dataClass: #MIXED
    },
    representativeKey: 'TravelStatus',
    resultSet.sizeCategory: #XS
}

define view entity ZRAPH_I_OverallStatus
  as select from DDCDS_CUSTOMER_DOMAIN_VALUE( p_domain_name: '/DMO/OVERALL_STATUS')

  association [0..*] to ZRAPH_I_OverallStatusText as _Text on $projection.TravelStatus = _Text.TravelStatus
{
      @ObjectModel.text.association: '_Text'
  key cast ( substring( value_low, 1, 1 ) as /dmo/overall_status preserving type ) as TravelStatus,
      _Text
}
